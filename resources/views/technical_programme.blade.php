@extends('layouts.master')

@section('title', 'BESC 2019 | Technical Programme')

@section('content')

<!-- Post Content Column -->
<div class="col-lg-8 post-wrapper">
    <!-- Post -->
        <div class="post">
            <h1 class="post-title">BESC 2019 Best Paper Award</h1>
                <ul>
                    <li><b>Best Research Paper</b></li>
                    Dynamics of social roles in the context of group evolution in the blogosphere
                    Jaroslaw Kozlak, Anna Zygmunt, Bogdan Gliwa and Krzysztof Rudek
                    <li><b>Distinguished Resarch on User Modeling, Privacy, and Ethics</b></li>
                    Dynamics of social roles in the context of group evolution in the blogosphere
                    Jaroslaw Kozlak, Anna Zygmunt, Bogdan Gliwa and Krzysztof Rudek
                    <li><b>Distinguished Resarch on Behavioral and Economic Computing</b></li>
                    Detection of Factors Influencing Market Liquidity Using an Agent-based Simulatione
                    Isao Yagi, Yuji Masuda and Takanobu Mizuta
                    <li><b>Distinguished Resarch on Digital Humanities</b></li>
                    Discovering Latent Psychological Structures from Self-report Assessments of Hospital Workers
                    Hsien-Te Kao, Homa Hosseinmardi, Shen Yan, Michelle Hasan, Shrikanth Narayanan, Kristina Lerman and Emilio Ferrara
                    <li><b>Distinguished Resarch on Social Computing and Applications</b></li>
                    Integrating Socio-Affective Information in Physical Perception aimed to Telepresence Robots
                    Ambre Davat, Veronique Auberge and Gang Feng
                    <li><b>Distinguished Resarch on Information Management and Systems</b></li>
                    The combination of context information to enhance simple question answering
                    Zhaohui Chao and Lin Li
                </ul>
                <hr>
            <h1 class="post-title">BESC 2018 Program</h1>
                <p>You can download the full program <a href="">here</a></p>
                <hr>
            <h1 class="post-title">Social Program</h1>
                <h2>Conference Reception</h2>
                    <p>
                    Buffet Style Reception<br>
                    November/12/2018 18:00-20:30<br> 
                    Garden Villa Hotel Banquet Hall<br>
                    <img src="img/g1.jpg" width="35%">
                    </p>
                <h2>Conference Banquet</h2>
                <p>Chinese Style Banquet 
                November/13/2018 18:30-21:30<br> 
                (Best paper award and presentation of BESC 2019 in Beijing)<br>
                <a href="https://www.linpalace.com/">THE LIN Palace</a><br>
                No. 99, BO-AI 2nd Road, Kaohsiung City, Taiwan<br>
                Find it in Google Map<br>   
                <img src="img/b1.jpg" width="35%">
                <img src="img/b2.jpg" width="35%">
                </p>
            <h1 class="post-title">Special Sessions</h1>
                <ul>
                    <li><a href="">Special Session on Wireless and Network Security (WNS 2018)</a></li>
                    <b>Special Session Chair:</b> Aneel Rahim, Dublin Institute of Technology Dublin, Ireland
                    <li><a href="">Special Session on Financial Data Modeling and Analytic Techniques</a></li>  
                    <b>Special Session Chair:</b> Chun-Hao Chen, Tamkang University, Taiwan
                </ul>
  
        </div>
</div>          
          
          
@endsection



