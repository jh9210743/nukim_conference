@extends('layouts.master')

@section('title', 'BESC 2019 | Registration')

@section('content')

    <!-- Post Content Column -->
    <div class="col-lg-8 post-wrapper">

        <h1>Registration</h1>
    
        <!-- Registration -->
        <div class="post">
            <p>Online Registration and Payment <i class="fas fa-arrow-right ml-1"></i> <a href="#">Online Registration System</a></p>

            <!-- Registration fee Table-->
            <table class="table table-bordered">
            <caption>Registration fee</caption>
    <thead>
        <tr>
        <th scope="col"></th>
        <th scope="col">Early Registration
            <br><span class="date">(By Sep. 30, 2018)</span></th>
        <th scope="col">Late Registration
            <br><span class="date">(After Sep. 30, 2018)</span></th>
        </tr>
    </thead>
    <tbody>
        <tr>
        <th scope="row">Regular  <br/>Registration</th>
        <td>16,000 NTD  <br/>(Approx. 500 USD)</td>
        <td>17,000 NTD  <br/>(Approx. 550 USD)</td>
        </tr>
        <tr>
        <th scope="row">Member Registration <br/>(IEEE, TASN members)</th>
        <td>14,000 NTD <br/>(Approx. 450 USD)</td>
        <td>16,000 NTD <br/>(Approx. 500 USD)</td>
        </tr>
    </tbody>
    </table>
        <p>BESC registration includes admission to all sessions, coffee breaks, and IEEE proceedings.
*If you are a student no-author or if your are an author but someone has already paid regular registration fee for the paper, you can contact us to ask about student registration (9500 NTD, Approx. 300 USD).</p>
        <p>For author who has second or third paper, additonal 4,600 NTD (150 USD) needs to be paid. For author who has fourth paper accepted, needs to pay another full registration fee.
<b>Please note: All registration fee are nonrefundable for authors.</b></p>
    </div>

    </div>

@endsection