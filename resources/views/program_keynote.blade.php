@extends('layouts.master')

@section('title', 'BESC 2019 | Keynotes')

@section('content')

<!-- Post Content Column -->
<div class="col-lg-8 post-wrapper">
    <!-- Post -->
        <div class="post">
            
            <!-- Title -->
            <h1 class="post-title mt-4">Keynotes</h1>
           
            <!-- Post Content -->
            <!-- Speaker -->
            <div class="col-lg-12 col-sm-6 text-center mt-4 mb-4 speaker">
              <img class="rounded-circle img-fluid d-block mx-auto" src="/img/ycz.jpg" alt="">
              <h4 class="mt-2"><a href="https://www.vu.edu.au/contact-us/yanchun-zhang">Prof. Yanchun Zhang</a></h4>
              <span>Director, Center for Applied Informatics <br/>Victoria University, Australia</span>
            </div>
            <hr>

            <!-- Speaker -->
            <div class="col-lg-12 col-sm-6 text-center mt-4 mb-4 speaker">
              <img class="rounded-circle img-fluid d-block mx-auto" src="/img/sd2.png" alt="">
              <h4 class="mt-2"><a href="https://www.cse.cuhk.edu.hk/irwin.king/home">Prof. Irwin King</a></h4>
              <span>The Chinese University of Hong Kong, Hong Kong</span>
            </div>
            <hr>

            <!-- Speaker -->
            <div class="col-lg-12 col-sm-6 text-center mt-4 mb-4 speaker">
              <img class="rounded-circle img-fluid d-block mx-auto" src="/img/king.png" alt="">
              <h4 class="mt-2"><a href="https://www.csie.ntu.edu.tw/~sdlin/">Prof. Shou-De Lin</a></h4>
              <span>National Taiwan University, Taiwan</span>
            </div>
            <hr>  
        </div>
</div>          
          
          
@endsection



