@extends('layouts.master')

@section('title', 'BESC 2019')

@section('content')

    <!-- Post Content Column -->
    <div class="col-lg-8 post-wrapper">

          <!-- What is New -->
          <div class="post">
            
            <!-- Title -->
            <h1 class="post-title">What is New</h1>
           
            <!-- Post Content -->
            <p class="lead">

              <ul class="list">
                <li>2018/11/24: Best Paper Awards of BESC 2018
                </li>
                <li>2018/10/10:
                  <a href="http://" target="_blank" rel="noopener noreferrer">Downloadable Program of BESC 2018</a>
                </li>
                <li>2018/10/4: Social Program of BESC 2018</li>
                <li>2018/10/2: Accepted student travel award announced: <a href="http://" target="_blank" rel="noopener noreferrer">Student Travel Award Awardees</a></li>
                <li>2018/09/28: Accepted paper list announced: <a href="http://" target="_blank" rel="noopener noreferrer">Accepted Papers</a></li>
                <li>2018/09/11: Online Registration is Open: <a href="http://" target="_blank" rel="noopener noreferrer">Online Registration System</a></li>
                <li>2018/07/01: Paper submission deadline extended to 2018/7/31</li>
                <li>2018/05/31: <a href="http://" target="_blank" rel="noopener noreferrer">Special Session on Financial Data Modeling and Analytic Techniques</a> , Special Session Chair: Chun-Hao Chen, Tamkang University, Taiwan</li>
                <li>2018/05/28: <a href="http://" target="_blank" rel="noopener noreferrer">Special Session on Social Computing for Business Intelligence and Analytics (SC-BI&A 2018)</a> , Special Session Chair: Muhammad Zubair Asghar, Institute of Computing and Information Technology, Gomal University D.I.Khan, KP, Pakistan</li>
                <li>2018/05/07: <a href="http://" target="_blank" rel="noopener noreferrer">Special Session on Wireless and Network Security (WNS 2018)</a>, Special Session Chair: Aneel Rahim, Dublin Institute of Technology, Dublin, Ireland</li>
                <li>2018/04/21: <a href="http://" target="_blank" rel="noopener noreferrer">Call for Student Travel Award</a></li>
                <li>2018/04/20: BESC 2018 Submission System Open  <a href="http://" target="_blank" rel="noopener noreferrer"> https://easychair.org/conferences/?conf=besc2018</a> </li>
                <li>2017/09/27: BESC 2018 Website is launched</li>
              </ul>

            </p>
            <!-- <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ut, tenetur natus doloremque laborum quos iste ipsum rerum obcaecati impedit odit illo dolorum ab tempora nihil dicta earum fugiat. Temporibus, voluptatibus.</p>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eos, doloribus, dolorem iusto blanditiis unde eius illum consequuntur neque dicta incidunt ullam ea hic porro optio ratione repellat perspiciatis. Enim, iure!</p> -->

          </div>

          <hr/>

          <!-- Welcome to BESC 2019 -->
          <div class="post">
            
            <!-- Title -->
            <h1 class="post-title">Welcome to BESC 2019</h1>

            <!-- Author -->
            <!-- <p class="lead">
              by
              <a href="#">Start Bootstrap</a>
            </p> -->

            <!-- Date/Time -->

            <!-- Preview Image -->
            <!-- <img class="img-fluid rounded" src="http://placehold.it/900x300" alt=""> -->
            <!-- Post Content -->
            <!-- <p class="lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus, vero, obcaecati, aut, error quam sapiente nemo saepe quibusdam sit excepturi nam quia corporis eligendi eos magni recusandae laborum minus inventore?</p> -->

            <p>The 5th International Conference on Behavioral, Economic, and Socio-Cultural Computing (BESC2018) will take place in Garden Villa Hotel, Kaohsiung, Taiwan, 12-14 November, 2018. The conference is organized by National University of Kaohsiung, Taiwan.
            </p>
            <p>BESC aims to become a premier forum in which academic researchers and industry practitioners from data mining, artificial intelligence, statistics and analytics, business and marketing, finance and politics, and behavioral, economic, social and psychological sciences could present updated research efforts and progresses on foundational and emerging interdisciplinary topics of BESC, exchange new ideas and identify future research directions.
            </p>
            <p>All accepted conference papers will be submitted for inclusion into IEEE Xplore as well as other Abstracting and Indexing (A&I), e.g. EI and DBLP. Top quality papers after presented in the conference will be selected for extension and publication in several special issues of international journals, e.g., World Wide Web Journal (Springer) Social Network Analysis and Mining (Springer) and Web Intelligence.
            </p>

            <!-- <blockquote class="blockquote">
              <p class="mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
              <footer class="blockquote-footer">Someone famous in
                <cite title="Source Title">Source Title</cite>
              </footer>
            </blockquote> -->
            <!-- <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Error, nostrum, aliquid, animi, ut quas placeat totam sunt tempora commodi nihil ullam alias modi dicta saepe minima ab quo voluptatem obcaecati?</p>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Harum, dolor quis. Sunt, ut, explicabo, aliquam tenetur ratione tempore quidem voluptates cupiditate voluptas illo saepe quaerat numquam recusandae? Qui, necessitatibus, est!</p> -->

          </div>

          <hr/>

          <!-- Journal Sepcial Issues -->
          <div class="post">
            
            <!-- Title -->
            <h1 class="post-title">Journal Sepcial Issues</h1>

            <p>Selected for extension and publication in several special issues of international journals</p>
            <ul>
              <li>World Wide Web Journal (Springer)</li>
              <li>Social Network Analysis and Mining (Springer)</li>
              <li>Web Intelligence</li>
              <li>Information Discovery and Delivery</li>
            </ul>

          </div>

          <hr/>

          <!-- Speaker Post -->
          <div class="post row">

            <!-- Title -->
            <div class="col-lg-12">
              <h1 class="post-title">Keynote Speakers</h1>
            </div>

            <hr/>

            <!-- Speaker -->
            <div class="col-lg-4 col-sm-6 text-center mt-4 mb-4 speaker">
              <img class="rounded-circle img-fluid d-block mx-auto" src="/img/ycz.jpg" alt="">
              <h5 class="mt-2"><a href="https://www.vu.edu.au/contact-us/yanchun-zhang">Prof. Yanchun Zhang</a></h5>
              <span>Director, Center for Applied Informatics <br/>Victoria University, Australia</span>
            </div>

            <!-- Speaker -->
            <div class="col-lg-4 col-sm-6 text-center mt-4 mb-4 speaker">
              <img class="rounded-circle img-fluid d-block mx-auto" src="/img/king.png" alt="">
              <h5 class="mt-2"><a href="https://www.cse.cuhk.edu.hk/irwin.king/home">Prof. Irwin King</a></h5>
              <span>The Chinese University of Hong Kong, Hong Kong</span>
            </div>

            <!-- Speaker -->
            <div class="col-lg-4 col-sm-6 text-center mt-4 mb-4 speaker">
              <img class="rounded-circle img-fluid d-block mx-auto" src="/img/sd2.png" alt="">
              <h5 class="mt-2"><a href="https://www.csie.ntu.edu.tw/~sdlin/">Prof. Shou-De Lin</a></h5>
              <span>National Taiwan University, Taiwan</span>
            </div>
            
          </div>

    </div>

    <hr/>

@endsection