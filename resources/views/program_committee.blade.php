@extends('layouts.master')

@section('title', 'BESC 2019 | Program Committee')

@section('content')

    <!-- Post Content Column -->
    <div class="col-lg-8 post-wrapper">
    
        <!-- Program Committee -->
        <div class="post">
            <h1 class="post-title">Program Committee</h1>
            <ul>
                <li>Esther Andres, INTA, Spain</li>
                <li>Cristina Baroglio, Dipartimento di Informatica, Universita di Torino, Italy</li>
                <li>Jason Barr, Rutgers University, USA</li>
                <li>Pietro Battiston, Scuola Superiore Sant'Anna, Pisa, Italy</li>
                <li>Michael Berger, Docuware AG, Germany</li>
                <li>Yi Cai, School of Software Engineering, South China University of Technology, China</li>
                <li>Hongmin Cai, South China University of Technology, China</li>
                <li>Yi Cai, South China University of Technology, China</li>
                <li>Myong-Hun Chang, Cleveland State University, USA</li>
                <li>Bin-Tzong Chie, Tamkang University, Taiwan</li>
                <li>German Creamer, Stevens Institute of Technology, USA</li>
                <li>Hasan Davulcu, Arizona State University, USA</li>
                <li>Herbert Dawid Bielefeld University, Germany</li>
                <li>J. Andres Diaz-Pace, ISISTAN Research Institute, UNICEN University, Argentina</li>
                <li>Svitlana Galeshchuk, Nova Southeastern University, USA</li>
                <li>Jianbo Gao</li>
                <li>Shyam Gouri Suresh, Davidson College, USA</li>
                <li>Shizheng Huang, Guangdong University of Petrochemical Technology, China</li>
                <li>Marianne Huang</li>
                <li>Martin Kollingbaum, University of Aberdeen, UK</li>
                <li>Jaroslaw Kozlak, AGH University of Science and Technology, Poland</li>
                <li>Chris Kuhlman, Virginia Tech, USA</li>
                <li>Krzysztof Kulakowski, AGH University of Science and Technology, Poland</li>
                <li>Xin Li, University of Science and Technology of China, China</li>
                <li>Yidong Li, Beijing Jiaotong University, China</li>
                <li>Hungwen Lin, Nanfang College of Sun Yat-Sen University, China</li>
                <li>Jerry Chun-Wei Lin, Harbin Institute of Technology Shenzhen Graduate School, China</li>
                <li>Hung-Wen Lin, Nanfang College of Sun Yat-Sen University Business School, China</li>
                <li>Shaowu Liu, University of Technology, Sydney, Australia</li>
                <li>Yu Liu, Beijing University of Posts and Telecommunications, China</li>
                <li>Kevin Macnish University of Twente, Netherlands</li>
                <li>Robert Marcjan, AGH University of Science and Technology, Poland</li>
                <li>Eric Matson, Purdue University, USA</li>
                <li>Jose M. Molina, Universidad Carlos III de Madrid, Spain</li>
                <li>Mikolaj Morzy, Poznan University of Technology, Poland</li>
                <li>Klaus Mueller, AGH University of Science & Technology, Poland</li>
                <li>Federico Neri, Harman International - Accutor, Italy</li>
                <li>Krzysztof Pietrowicz, Nicolaus Copernicus University, Poland</li>
                <li>Agostino Poggi, University of Parma, Italy</li>
                <li>Aneel Rahim</li>
                <li>Alex Redlein, Vienna University of Technology (TU Vienna) / IFM, Austria</li>
                <li>Christopher Ruebeck, Lafayette College, USA</li>
                <li>Abida Sadaf</li>
                <li>Silvia Schiaffino ISISTAN, Instituto Superior de Ingenier??a de Software Tandil (CONICET - UNCPBA), Argentina</li>
                <li>Shafqat Shad Maharishi University of Management, USA</li>
                <li>Ali Shahrabi Glasgow Caledonian University, UK</li>
                <li>Jun Shen University of Wollongong, Australia</li>
                <li>Gerardo Simari, Universidad Nacional del Sur and CONICET, Argentina</li>
                <li>Chung-Ching Tai</li>
                <li>Patrick Taillandier</li>
                <li>Xiaohui Tao University of Southern Queensland, Australia</li>
                <li>I-Hsien Ting, National University of Kaohsiung, Taiwan</li>
                <li>Marijana Tomic, University of Zadar, Croatia</li>
                <li>Paolo Torroni, University of Bologna, Italy</li>
                <li>Chueh-Yung Tsao, Chang Gung University, Taiwan</li>
                <li>Iraklis Varlamis, Harokopio University of Athens, Greece</li>
                <li>Xin Wang, University of Calgary, Canada</li>
                <li>Katarzyna Wegrzyn-Wolska, ESIGETEL, France</li>
                <li>Haoran Xie The Education University of Hong Kong</li>
                <li>Guandong Xu, University of Technology, Sydney, Australia</li>
                <li>Hsin-Chang Yang, National University of Kaohsiung, Taiwan</li>
                <li>Tzu-Hsien Yang, National University of Kaohsiung, Taiwan</li>
                <li>Kun Yue, Yunnan University, China</li>
                <li>Si Zhang, Arizona State University, USA</li>
                <li>Yuan Zhang, Nanjing University, China</li>
                <li>Wenping Zhang, Renmin University, China</li>
                <li>Yao Zhou, Arizona State University, USA</li>
                <li>Shang-Ming Zhou, Swansea University, UK</li>
                <li>Xujuan Susan Zhou, UNIVERSITY OF SOUTHERN QUEENSLAND, Australia</li>
                <li>Jiangnan Zhu, The University of Hong Kong</li>
                <li>Tingshao Zhu, Institute of Psychology, Chinese Academy of Sciences, China</li>
                <li>Jianke Zhu, Zhejiang University, China</li>
                <li>Anna Zygmunt, AGH, Poland</li>
            </ul>
        </div>

    </div>

@endsection