@extends('layouts.master')

@section('title', 'BESC 2019 | Call for Papers')

@section('content')

    <!-- Post Content Column -->
    <div class="col-lg-8 post-wrapper">

        <!-- Call for Papers -->
        <h1>Call for Papers</h1>
        <p>(Downloads: <a href="http://besc-conf.org/2018/files/BESC2018-CFP.txt" target="_blank">CFP in TXT</a> / <a href="http://besc-conf.org/2018/files/besc2018-flyer.pdf" target="_blank">Flyer in PDF</a>)</p>

        <!-- Conference Scope -->
        <div class="post">
            <h2>Conference Scope</h2>
            <p>The 5th International Conference on Behavioral, Economic, and Socio-Cultural Computing (BESC2018) will take place in Garden Villa Hotel, Kaohsiung, Taiwan, 12-14 November, 2018. The conference is organized by National University of Kaohsiung, Taiwan.</p>
            <p>BESC aims to become a premier forum in which academic researchers and industry practitioners from data mining, artificial intelligence, statistics and analytics, business and marketing, finance and politics, and behavioral, economic, social and psychological sciences could present updated research efforts and progresses on foundational and emerging interdisciplinary topics of BESC, exchange new ideas and identify future research directions.</p>
            <p>All accepted conference papers will be submitted for inclusion into IEEE Xplore as well as other Abstracting and Indexing (A&I), e.g. EI and DBLP. Top quality papers after presented in the conference will be selected for extension and publication in several special issues of international journals, e.g., World Wide Web Journal (Springer) Social Network Analysis and Mining (Springer) and Web Intelligence.</p>
            <ul class="list">
               <li><a href="http://link.springer.com/journal/11280" target="_blank" rel="noopener noreferrer">World Wide Web Journal</a> (Springer)</li>
               <li><a href="http://www.springer.com/journal/13278/" target="_blank" rel="noopener noreferrer">Social Network Analysis and Mining</a> (Springer)</li>
            </ul>
            <p>The conference committee invites submissions of applied or theoretical research and application-oriented papers on any below areas/tracks.</p>
        </div>
        <hr/>

        <!-- Topics -->
        <div class="post">
            <h2 class="post-title">Topics</h2>
            <p>Papers are invited in the areas below, but do not exclude research in the general areas of the topic headings.</p>
            <!-- 2 Grids -->
            <div class="row">
                <div class="col-lg-6 col-sm-12">
                    <h4>Social Computing and Applications</h4>
                    <hr>
                    <ul>
                        <li>Social behavior</li>
                        <li>Social network analysis</li>
                        <li>Semantic web</li>
                        <li>Collective intelligence</li>
                        <li>Security, privacy, trust in social contexts</li>
                        <li>Social commerce and related applications</li>
                        <li>Social recommendation</li>
                        <li>Social data mining</li>
                    </ul>
                </div>
                <div class="col-lg-6 col-sm-12">
                    <h4>Behavioral and Economic Computing</h4>
                    <hr>
                    <ul>
                        <li>Agent-based modeling</li>
                        <li>Artificial/experimental markets</li>
                        <li>Asset pricing</li>
                        <li>Computational finance</li>
                        <li>Financial crises</li>
                        <li>Monetary policy</li>
                        <li>Optimization</li>
                        <li>Volatility modeling</li>
                        <li>Evolutionary economics</li>
                    </ul>
                </div>
                <div class="col-lg-6 col-sm-12">
                    <h4>Information Management and Systems</h4>
                    <hr>
                    <ul>
                        <li>Decision analytics</li>
                        <li>E-Business</li>
                        <li>Societal impacts of IS</li>
                        <li>Human behavior and IS</li>
                        <li>IS in healthcare</li>
                        <li>IS security and privacy</li>
                        <li>IS strategy, structure and organizational impacts</li>
                        <li>Service science and IS</li>
                    </ul>
                </div>
                <div class="col-lg-6 col-sm-12">
                    <h4>Digital Humanities</h4>
                    <hr>
                    <ul>
                        <li>Digital media</li>
                        <li>Digital humanities</li>
                        <li>Digital games and learning</li>
                        <li>Digital footprints and privacy</li>
                        <li>Twitter histories creation</li>
                        <li>Crowd dynamics</li>
                        <li>Digital arts</li>
                        <li>Digital healthcare</li>
                        <li>Mobile technologies</li>
                        <li>Activity streams and experience design</li>
                    </ul>
                </div>
                <div class="col-lg-6 col-sm-12">
                    <h4>User Modeling, Privacy, and Ethics</h4>
                    <hr>
                    <ul>
                        <li>Personalization for individuals, groups and populations</li>
                        <li>Large scale personalization, adaptation and recommendation</li>
                        <li>Web dynamics and personalization</li>
                        <li>Privacy, perceived security and trust</li>
                    </ul>
                </div>
            </div>
        </div>
        <hr/>

        <!-- Key Dates -->
        <div class="post">
            <h2 class="post-title">Key Dates</h2>
            <ul class="list">
               <li><b>Papers due:31/07/2018 (Extended)</b></li>
               <li>Notification due:9/09/2018</li>
               <li>Camera-ready due:30/09/2018</li>
               <li>Conference date:12-14/11/2018</li>           
            </ul>
        </div>
        <hr/>

        <!-- Special Session -->
        <div class="post">
            <h2 class="post-title">Special Session</h2>
            <ul>
               <li>
                   <p>2018/05/28: 
                    <a href="http://besc-conf.org/2018/files/SC-BIA-2018.doc" target="_blank" rel="noopener noreferrer">Special Session on Social Computing for Business Intelligence and Analytics (SC-BI&A 2018)</a>, 
                    <span class="chair">Special Session Chair:</span> Muhammad Zubair Asghar, Institute of Computing and Information Technology, Gomal University D.I.Khan, KP, Pakistan
                   </p>
                </li>
                <li>
                   <p>2018/05/07: 
                    <a href="http://besc-conf.org/2018/files/SC-BIA-2018.doc" target="_blank" rel="noopener noreferrer">Special Session on Wireless and Network Security (WNS 2018)</a>, 
                    <span class="chair">Special Session Chair:</span> Aneel Rahim, Dublin Institute of Technology, Dublin, Ireland
                   </p>
                </li>
            </ul>
        </div>
        <hr/>

        <!-- Paper Submission -->
        <div class="post">
            <h2 class="post-title">Paper Submission</h2>
            <p>All papers will be reviewed by the Program Committee on the basis of technical quality, relevance to BESC 2018, originality, significance and clarity. Please note:</p>
            <ul>
                <li>All submissions should use IEEE two-column style. Templates are available from <a href="http://www.ieee.org/conferences_events/conferences/publishing/templates.html" target="_blank">here.</a></li>
                <li>All papers must be submitted electronically through the paper submission system in PDF format only; BESC2018 accepts <span class="em">scientific papers (6 pages), short papers (2 pages) and demo papers (2 pages).</span></li>
                <li>Submitted papers must not substantially overlap with papers that have been published or that are simultaneously submitted to a journal or a conference with proceedings.</li>
                <li>Papers must be clearly submitted in English and will be selected based on their originality, timeliness, significance, relevance, and clarity of presentation.</li>
                <li>Submission of a paper should be regarded as a commitment that, should the paper be accepted, at least one of the authors will register and attend the conference to present the work.</li>   
            </ul>
            <p>Paper submission system is available at: <a href="https://easychair.org/conferences/?conf=besc2018" target="_blank">https://easychair.org/conferences/?conf=besc2018</a></p>
        </div>
        <hr/>

        <!-- Further Information -->
        <div class="post">
            <h2 class="post-title">Further Information</h2>
            <p>For all inquiries, please contact</p>
            <p><i class="fas fa-envelope"></i>
                Email: <a href="mailto:IEEE-BESC@gmail.com">IEEE-BESC@gmail.com</a>
            </p>
        </div>
          
    </div>

@endsection