@extends('layouts.master')

@section('title', 'BESC 2019 | Paper Submission')

@section('content')

    <!-- Post Content Column -->
    <div class="col-lg-8 post-wrapper">

        <h1>Paper Submission</h1>
    
        <!-- Paper Submission -->
        <div class="post">
            <p>All papers will be reviewed by the Program Committee on the basis of technical quality, relevance to BESC 2018, originality, significance and clarity. Please note:</p>
            <ul>
               <li>All submissions should use IEEE two-column style. Templates are available from <a href="http://www.ieee.org/conferences_events/conferences/publishing/templates.html" target="_blank" rel="noopener noreferrer">here.</a></li>
                <li>All papers must be submitted electronically through the paper submission system in PDF format only, BESC2018 accepts scientific papers (6 pages), short papers (2 pages) and demo papers (2 pages).</li>
                <li>Submitted papers must not substantially overlap with papers that have been published or that are simultaneously submitted to a journal or a conference with proceedings.</li>
                <li>Papers must be clearly presented in English and will be selected based on their originality, timeliness, significance, relevance, and clarity of presentation.</li>
                <li>Submission of a paper should be regarded as a commitment that, should the paper be accepted, at least one of the authors will register and attend the conference to present the work.</li>
            </ul>
            <p>Paper submission system is available at: <a href="https://easychair.org/conferences/?conf=besc2018" target="_blank" rel="noopener noreferrer">https://easychair.org/conferences/?conf=besc2018</a></p>
        </div>

    </div>

@endsection