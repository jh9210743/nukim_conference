@extends('layouts.master')

@section('title', 'BESC 2019 | Call for Industry Track')

@section('content')

    <!-- Post Content Column -->
    <div class="col-lg-8 post-wrapper">

        <h1>Industry Track CFP</h1>
    
        <!-- Conference Scope -->
        <div class="post">
            <h2 class="post-title">Conference Scope</h2>
            <p>The 5th International Conference on Behavioral, Economic, and Socio-Cultural Computing (BESC2018) will take place in Garden Villa Hotel, Kaohsiung, Taiwan, 12-14 November, 2018. The conference is organized by National University of Kaohsiung, Taiwan.</p>
            <p>The Industry Track of 2018 International Conference on Behavioral, Economic, and Socio-Cultural Computing (BESC 2018) seeks submissions in all submission categories (6-page full paper, 2-page short paper, and 2-page demo paper) to showcase emerging techniques and advanced ideas in industry, to discuss future trends and challenges with practical implication, and to facilitate interactions between academia and industry.</p>
            <p>All papers submitted to the BESC 2018 Industry Track will be peer-reviewed and should not be submitted to another conference at the same time. Compared to the BESC 2018 regular track, Industry Track papers do not necessarily need to contain major methodological advances, algorithmic contributions, or rigorous experimental validation. Rather, the emphasis will be on industrial innovations, clear practical relevance, interesting lessons learned through significant practice, or new classes of applications which might open up new venues for interesting academic work. Works-in-progress papers are also welcomed. Contributions from both industry R&D groups and the academia are welcome.</p>
        </div>
        <hr/>

        <!-- Topics -->
        <div class="post">
            <h2 class="post-title">Topics</h2>
            <p>We welcome submissions on the following, but not limited to, topics: </p>
            <ul>
               <li>Data mining, artificial intelligence, statistical analytics in industry</li>
               <li>Consumer-driven data modeling, analysis and visualization</li>
               <li>Emerging applications in economic, marketing, and finance areas</li>
               <li>Methodologies and frameworks arising from big data with direct industry relevance</li>
               <li>Machine learning and prediction from big data</li>
               <li>Architectures and frameworks for building large, scalable and powerful systems for processing and analyzing data</li>
               <li>Social analysis, socio studies, psychological and behavioral descriptions</li>
               <li>Consumer churn, consumer behaviors, and consumer forecasting</li>
               <li>Experiments and case studies using industry collected data</li>              
            </ul>
        </div>
        <hr/>

        <!-- Key Dates -->
        <div class="post">
            <h2 class="post-title">Key Dates</h2>
            <ul class="list">
               <li><b>Papers due:31/07/2018 (Extended)</b></li>
               <li>Notification due:9/09/2018</li>
               <li>Camera-ready due:30/09/2018</li>
               <li>Conference date:12-14/11/2018</li>   
            </ul>
        </div>
        <hr/>

        <!-- Industry Track Paper Submission -->
        <div class="post">
            <h2 class="post-title">Industry Track Paper Submission</h2>
            <ul>
               <li>
                  All submissions should use IEEE two-column style. Templates are available from
                  <a href="http://www.ieee.org/conferences_events/conferences/publishing/templates.html" target="_blank" rel="noopener noreferrer">http://www.ieee.org/conferences_events/conferences/publishing/templates.html</a>
                </li>
               <li>All papers must be submitted electronically through the paper submission system in PDF format only.</li>
               <li>Submitted papers must not substantially overlap with papers that have been published or that are simultaneously submitted to a journal or a conference with proceedings.</li>
               <li>Papers must be clearly presented in English and will be selected based on their originality, timeliness, significance, relevance, and clarity of presentation.</li>
               <li>Submission of a paper should be regarded as a commitment that, should the paper be accepted, at least one of the authors will register and attend the conference to present the work.</li>
            </ul>
            <p>Paper submission system is available at: <a href="https://easychair.org/conferences/?conf=besc2018" target="_blank" rel="noopener noreferrer">https://easychair.org/conferences/?conf=besc2018</a></p>
        </div>
        <hr/>

        <!-- Further Information -->
        <div class="post">
            <h2 class="post-title">Further Information</h2>
            <p>If you have any questions, please email the Industry Track co-chairs at:
            <ul class="no-style">
                <li><i class="fas fa-envelope"></i>
                Xin Li: <a href="mailto:xinli2@iflytek.com">xinli2@iflytek.com</a></li>
                <li><i class="fas fa-envelope"></i>
                Alvin Chin: <a href="mailto:alvin.chin@bmwna.com">alvin.chin@bmwna.com</a></li>
            </ul>
        </div>

    </div>

@endsection