@extends('layouts.master')

@section('title', 'BESC 2019 | Venue & Accommodation')

@section('content')

    <!-- Post Content Column -->
    <div class="col-lg-8 post-wrapper">

        <h1>Hotel Information</h1>
    
        <!-- Hotel Information -->
        <div class="post">
            <p class="text-left">Garden Villa Hotel, Kaohsiung (the conference venue) has reserved a block of rooms for BESC 2018 at a special rate. We will announce the special rate soon. <a href="http://www.gardenvilla.com.tw/eindex.php" target="_blank">Garden Villa Hotel</a></p>
            <p><a href="http://besc-conf.org/2018/files/gv.pdf">Garden Villa Hotel Reservation Form</a></p>

            <div class="row">
                <div class="col-lg-4 col-sm-12"><img class="img-fluid" src="img/hotel1.jpg" alt="hotel1"></div>
                <div class="col-lg-4 col-sm-12"><img class="img-fluid" src="img/hotel2.jpg" alt="hotel2"></div>
                <div class="col-lg-4 col-sm-12"><p>Other recommended hotels</p>
                    <ul>
                        <li><a href="http://besc-conf.org/2018/files/Hanhsian.pdf" target="_blank">Han-Hsian Hotel</a></li>
                        <li><a href="http://www.hotelr14.com/" target="_blank">Hotel R14</a></li>
                        <li><a href="http://www.hotelr14.com/" target="_blank">Hotel H2O</a></li>
                        <li><a href="http://www.hotelr14.com/" target="_blank">Howard Hotel</a></li>
                    </ul>
                </div>
            </div>
        </div>

        <hr/>

        <h1>Conference Venue</h1>

        <!-- Conference Venue -->
        <div class="post">
            <p>The conference will take place at Garden Villa Hotel, Kaohsiung.</p>
            <p><strong>Address: </strong>No.801 Chongde Rd. Zuoying District, Kaohsiung City. Taiwan</p>
        </div>

    </div>

@endsection