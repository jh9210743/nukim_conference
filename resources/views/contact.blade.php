@extends('layouts.master')

@section('title', 'BESC 2019 | Contact')

@section('content')

    <!-- Post Content Column -->
    <div class="col-lg-8 post-wrapper">

        <h1>Confact Information</h1>

        <!-- Contact -->
        <div class="post">

            <!-- Confact Information  -->
            <div class="post">
                <p>For all inquiries, please contact</p>
                <p><i class="fas fa-envelope"></i>
                    Email: <a href="mailto:IEEE-BESC@gmail.com">IEEE-BESC@gmail.com</a>
                </p>
                <p>Alternative, you can contact the program co-chair Prof. I-Hsien Ting <a href="mailto:iting@nuk.edu.tw">iting@nuk.edu.tw</a></p>
            </div>

        </div>

    </div>

@endsection
