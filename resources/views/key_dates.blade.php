@extends('layouts.master')

@section('title', 'BESC 2019 | Key Dates')

@section('content')

    <!-- Post Content Column -->
    <div class="col-lg-8 post-wrapper">

        <h1>Key Dates</h1>
    
        <!-- Key Dates -->
        <div class="post">

            <table>
                <tr>
                    <td>
                    <ul class="list">
                        <li><b>Papers due:</b></li>
                        <li>Notification due:</li>
                        <li>Camera-ready due:</li>
                        <li>Conference date:</li>            
                    </ul>
                    </td>
                    <td>
                    <ul class="no-style txt-right">
                        <li><b>31/07/2018(Extended)</b></li>
                        <li>9/09/2018</li>
                        <li>30/09/2018</li>
                        <li>12-14/11/2018</li>             
                    </ul>
                    </td>
                </tr>
            </table>

            
        </div>

    </div>

@endsection