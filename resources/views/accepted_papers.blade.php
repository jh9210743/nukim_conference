@extends('layouts.master')

@section('title', 'BESC 2019 | Accepted Papers')

@section('content')

<!-- Post Content Column -->
<div class="col-lg-8 post-wrapper">
    <!-- Post -->
        <div class="post">
            <h1 class="post-title">BESC 2019 Best Paper Award</h1>
            <h2>Best Research Paper</h2>
                <ul>
                    <li>Dynamics of social roles in the context of group evolution in the blogosphere</li>
                    <li>Jaroslaw Kozlak, Anna Zygmunt, Bogdan Gliwa and Krzysztof Rudek</li>
                    <li>Distinguished Resarch on User Modeling, Privacy, and Ethics</li>
                    <li>Dynamics of social roles in the context of group evolution in the blogosphere</li>
                    <li>Jaroslaw Kozlak, Anna Zygmunt, Bogdan Gliwa and Krzysztof Rudek</li>
                    <li>Distinguished Resarch on Behavioral and Economic Computing</li>
                    <li>Detection of Factors Influencing Market Liquidity Using an Agent-based Simulatione</li>
                    <li>Isao Yagi, Yuji Masuda and Takanobu Mizuta</li>
                    <li>Distinguished Resarch on Digital Humanities</li>
                    <li>Discovering Latent Psychological Structures from Self-report Assessments of Hospital Workers</li>
                    <li>Hsien-Te Kao, Homa Hosseinmardi, Shen Yan, Michelle Hasan, Shrikanth Narayanan, Kristina Lerman and Emilio Ferrara</li>
                    <li>Distinguished Resarch on Social Computing and Applications</li>
                    <li>Integrating Socio-Affective Information in Physical Perception aimed to Telepresence Robots</li>
                    <li>Ambre Davat, Veronique Auberge and Gang Feng</li>
                    <li>Distinguished Resarch on Information Management and Systems</li>
                    <li>The combination of context information to enhance simple question answering</li>
                    <li>Zhaohui Chao and Lin Li
                </ul>
            <h2>BESC2018-Short Papers</h2>
                <ul>
                    <li>Awatif Al Habsi and Yara Al Kahala. Discrimination, Voice & Trust: An Experimental Approach</li>
                    <li>Moloud Abdar, Mariam Zomorodi-Moghadam and Xujuan Zhou. An Ensebmbel-based Decision Tree Approach for Educational Data Mining</li>
                    <li>Jiemin Zhong, Haoran Xie, Di Zou and Dickson K.W. Chui. A blockchain model for word-learning systems</li>
                    <li>Yu Zong, Xin Li and Guochen Li. A Multi-Spot Explode Clustering Algorithm By Using Heuristic Search</li>
                    <li>Ping Jin, Yu Zong and Guochen Li. MOSC A Multi-Objective Subspace Clustering Algorithm</li>
                    <li>Xu Ran Gao. Executive Incentive and R&D Investment-An Empirical Evidence from Chinese GEM Listed Companies</li>
                    <li>Hongxun Jiang, Mengjun Zhu and Caihong Sun. Exploring propagation factors of social media moods for stock prices prediction</li>
                    <li>Rui Zhang, Xiao Wang, Kezhong Liu, Xiaolie Wu, Jiongjiong Liu and Tianyou Lu. Ship Collision Avoidance Using Constrained Deep Reinforcement Learning</li>
                    <li>Abhishek Mahalle, Jianming Yong and Xiaohui Tao. Control over Incident Management Processes and Change Management Processes to mitigate Operations Risk in Cloud Architecture Infrastructure For Banking and Financial Services Industry</li>
                    <li>Yi-Wei Hsieh, Ching Li, Yu-Lan Yuan, Chung Yung and Hsiao-Chang Huang. A perspective of destination management on Web log data and the official website</li>
                    <li>Krzysztof Rudek and Jaroslaw Kozlak. Prediction of the persistence of relationships in social networks, considering previous reciprocity and duration</li>
                    <li>Takanobu Mizuta. Effect of Increasing Horizontal Shareholding with Index Funds on Competition and Market Prices -- Investigation by Agent-Based Model</li>
                    <li>Tingxin Wei and Weiguang Qu. Optimizing the Taxonomy and Hierarchy of a Chinese Lexical Database -- Cilin</li>
                    <li>Yi Liu, Xian Zhong, Lin Li, Jingling Yuan and Ruiqi Luo. A Novel Algorithm for Group Recommendation Based on Combination of Recessive Characteristics</li>
                    <li>Xin Yang, Jukai Hou and Xiajun Yi. Investor Overconfidence and Stock Price Crash Risk</li>
                    <li>Aiko Mieno, Kazuo Hemmi, Takuya Nagamine, Naomi Yamasumi, Hitomi Sakamoto and Eriko Yoshida. Development of Oral Function Improvement System Using Tablet Computer</li>
                    <li>Masahiro Morita, Kazuaki Naruse and Shiro Uesugi. A Study on Adopting Smart Payment System</li>
                    <li>Noora Alallaq, Muhmmad Al-Khiza'Ay and Xin Han. Detecting Suspicious Social Astroturfing Groups in Tourism Social Networks</li>
                    <li>Yun Liu. A Content Based Interest Mining Approach to Weibo Users</li>
                    <li>Xiong Zhou, Fang Zheng, Kc Chan, Xujuan Zhou, Raj Gururajan and Zhangguang Wu. Based on the Internet Plus the Safety of Agricultural Products Traceability Research</li>
                    <li>Hsin-Yi Chiu, Jia-Sing Lin and Chienhsing Wu. Uncovering Determinants of Discontinuous Intention of Attention to Mobile LINE-P Messages</li>
                    <li>Bang Wu, Zixiang Ma, Stefan Poslad and Yidong Li. WiFi Fingerprint Based, Indoor, Location-Driven, Activities of Daily Living Recognition</li>
                    <li>Jing Qiu, Lin Li and Yunpei Zheng. Micro-blog User Profiling: A Supervised Clustering based Approach for Age and Gender Classification</li>
                </ul> 
            <h2>Special Session:SC-BI&A 2018</h2>
                <ul>
                    <li>Iqra Sana, Khushboo Nasir, Amara Urooj, Zain Ishaq and Ibrahim A Hameed. BERS: Bussiness-Related Emotion Recognition System in Urdu Language Using Machine Learning</li>
                    <li>Anam Habib, Furqan Khan Saddozia, Anum Sattar, Aurangzeb Khan, Ibrahim A. Hameed and Fazal Masud Kundi. User Intention Mining in Bussiness Reviews: A Review WNS</li>
                    <li>Tahir Muhammad Ali, Imran Ahmad. Understanding Customer Experiences through Social Media Analysis of Three Giants of Soft Drink Industry</li>
                    <li>Tahir Muhammad Ali, Imran Ahmad. A Comparative Analysis of Top 5 Fast Food Restaurants through Text Mining</li> 
                </ul>
            <h2>Computational Social Science in Mainland China</h2>
                <ul>
                    <li>Jiaxing Chen, Yanghui Rao, Runxuan Chen, Zhongxuan Lin, Kaisheng Lai and Lingnan He. Attributed Network Embedding with Data Distribution Adaptation</li>
                    <li>Zhongxuan Lin, Meiqi Sun, Shuqing Gao, Lingnan He and Kaisheng Lai. The influence of economic development on prosocial tendency on social media base on big data analysis of 95 Chinese cities</li>
                    <li>Xling Xiong and Lingnan He. Collective Attention in WeChat Public Platform</li>
                    <li>Jian Dong, Bin Chen, Fang Zhang, Liang Liu, Chuan Ai and Danhuai Guo. Analysis of the Reasons for Unbalanced Distribution of Dockless Bike Sharing</li>
                    <li>Tian Lan and Tuo Liu. Differential Item Functioning (DIF) analysis for large social survey data: a method detecting valuable information in big-data era</li>
                    <li>Jing Zhou, Jianing Tang, Jue Zhou, Ziyue Zhu and Tian Xie. Materialistic vs. Meaningful Festival Celebration for Children on Sina Microblog</li> 
                </ul>
            <h2>Financial Data Modeling and Analytic Techniques</h2>
                <ul>
                    <li>Yu-Chuan Tsai, Shyue-Liang Wang, I-Hsien Ting and Tzung-Pei Hong. Flexible Anonymization of Transactions with Sensitive Items</li>
                    <li>Yu-Fei Lin, Wei-Ho Chung and Yeong-Luh Ueng. Stock pricing via Recurrent Neural Network with multi-step and first order difference approach</li>
                    <li>Mu-En Wu and Pang-Jen Hung. A Framework of Option Buy-Side Strategy with Simple Index Futures Trading Based on Kelly Criterion</li>
                    <li>Chun-Hao Chen, Bing-Yang Chiang and Tzung-Pei Hong. An Approach for Optimizing Group Stock Portfolio Using Multi-Objective Genetic Algorithm</li>
                </ul>           
        </div>
</div>          
          
          
@endsection



