@extends('layouts.master')

@section('title', 'BESC 2019 | Organizing Committee')

@section('content')

    <!-- Post Content Column -->
    <div class="col-lg-8 post-wrapper">
    
        <!-- Organizing Committee -->
        <div class="post">
            <h1 class="post-title">Organizing Committee</h1>
            <ul>
                <li>General Chair
                    <ul>
                        <li>Leon S. L. Wang, National University of Kaohsiung, Taiwan</li>
                        <li>Tzung-Pei Hong, National University of Kaohsiung, Taiwan</li>
                    </ul>
                </li>
                <li>Steering Committee Chair
                    <ul>
                        <li>Guandong Xu, University Techonlogy of Sydney, Australia</li>
                    </ul>
                </li>
                <li>Program Committee Co-chairs
                    <ul>
                        <li>I-Hsien Ting, National University of Kaohsiung, Taiwan</li>
                        <li>Xiaohui Tao, University of Southern Queensland, Australia</li>
                    </ul>
                </li>
                <li>Program Committee Co-chairs
                    <ul>
                        <li>Social Computing and Applications:
                            <ul>
                                <li>Jerry Lin, HIT-Shenzhen</li>
                            </ul>
                        </li>
                        <li>Behavioral and Economic Computing:
                            <ul>
                                <li>Shizheng Huang, Guangdong University of Petrochemical Technology, China</li>
                                <li>Hung-Wen Lin, Nanfang College of Sun Yat-Sen University Business School, China</li>
                                <li>Chih-Hsiang Chang, National University of Kaohsiung, Taiwan</li>
                            </ul>
                        </li>
                        <li>Information Management and Systems:
                            <ul>
                                <li>Yi Cai, South China University of Technology, China</li>
                                <li>Haoran Xie, The Education University of Hong Kong</li>
                                <li>Wenping Zhang, Renmin University, China</li>
                            </ul>
                        </li>
                        <li>Digital Humanities:
                            <ul>
                                <li>Jiangnan Zhu, University of Hong Kong</li>
                            </ul>
                        </li>
                        <li>User Modeling, Privacy, and Ethics:
                            <ul>
                                <li>Yidong Li, Beijing Jiaotong University, China</li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <li>Steering Committee Chair
                    <ul>
                        <li>Kai Wang, National University of Kaohsiung, Taiwan</li>
                    </ul>
                </li>
            </ul>
            <ul>          
                <li>Special Sesssion Chair</li>
                <li>Publication Chair
                    <ul>
                        <li>Hsing-Chang Yang, National University of Kaohsiung, Taiwan</li>
                    </ul>
                </li>
                <li>Publicity Chair
                    <ul>
                        <li>Been-Chian Chien, National University of Tainan, Taiwan</li>
                        <li>Yongrui (Louie) Qin, University of Huddersfield, UK</li>
                        <li>Haoran Xie, The Education University of Hong Kong, Hong Kong</li>
                        <li>Xujuan Zhou, University of Southern University, Australia</li>
                    </ul>
                </li>
                <li>Local Arrangement Chair
                    <ul>
                        <li>Kai Wang, National University of Kaohsiung, Taiwan</li>
                        <li>Tzu-Hsien Yang, National University of Kaohsiung, Taiwan</li>
                    </ul>
                </li>
            </ul>
        </div>

    </div>

@endsection