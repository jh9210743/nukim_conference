<!-- Bootstrap core JavaScript -->
<script src="{{ asset('js/plugins/jquery/jquery.min.js') }}"></script>
<script src="{{ asset('js/plugins/bootstrap/bootstrap.bundle.min.js') }}"></script>

<!-- Custom scripts for this template -->
<script src="{{ asset('js/clean-blog.js') }}"></script>

<!-- Menu -->
<script src="{{ asset('js/menu.js') }}"></script>
<script src="{{ asset('js/modernizr.custom.js') }}"></script>

<script>

    var menu = new cbpHorizontalSlideOutMenu( document.getElementById( 'cbp-hsmenu-wrapper' ) );
    var submenu_height = null;

    $(document).ready(function() {
        $(window).resize(function(){
            var $article = $('#main-article');
            var el = document.querySelector('.cbp-hsmenubg');
            var style = window.getComputedStyle ? getComputedStyle(el, null) : el.currentStyle;
            if(style.display === 'none')
                $article.css("top", "0px");
            else
                $article.css("top", style.height);
        });
    });

</script>