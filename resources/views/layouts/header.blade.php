<header>
      <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
      <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
      <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
      <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
      <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
      <li data-target="#carouselExampleIndicators" data-slide-to="4"></li>
      <li data-target="#carouselExampleIndicators" data-slide-to="5"></li>

    </ol>
    <div class="carousel-inner">
      <div class="carousel-item active">
        <img class="d-block w-100" src="img/slide/slide1.jpg" alt="First slide">
      </div>
      <div class="carousel-item">
        <img class="d-block w-100" src="img/slide/slide2.jpg" alt="Second slide">
      </div>
      <div class="carousel-item">
        <img class="d-block w-100" src="img/slide/slide3.jpg" alt="Third slide">
      </div>
      <div class="carousel-item">
        <img class="d-block w-100" src="img/slide/slide4.jpg" alt="Fourth slide">
      </div>
      <div class="carousel-item">
        <img class="d-block w-100" src="img/slide/slide5.png" alt="Fifth slide">
      </div>
    </div>
    <div class="carousel-caption d-md-block">
      <div class="site-heading">
                  <h1>BESC 2019</h1>
                  <span class="subheading">The 5th International Conference on Behavioral, Economic, and Socio-Cultural Computing 
    National University of Kaohsiung, Taiwan / 12-14 November, 2018</span>
      </div>
    </div>
  </div>
</header>


    <!-- Page Header -->
<!-- <header class="masthead" style="background-image: url('img/home-bg.jpg')">
      <div class="overlay"></div>
      <div class="container">
        <div class="row">
          <div class="col-lg-8 col-md-10 mx-auto">
            <div class="site-heading">
              <h1>BESC 2019</h1>
              <span class="subheading">The 5th International Conference on Behavioral, Economic, and Socio-Cultural Computing 
National University of Kaohsiung, Taiwan / 12-14 November, 2018</span>
            </div>
          </div>
        </div>
      </div>
    </header> -->