<!-- Sidebar -->

<!-- Sidebar Widgets Column -->
<div class="sidebar col-md-4">

<!-- Search Widget -->
<div class="card my-4 border-0">
  <h5 class="card-header"><i class="far fa-calendar"></i> Key Dates</h5>
  <div class="card-body">
    <div class="input-group">

    <ul class="list">
      <li>Papers due:31/07/2018 (Extended)</li>
      <li>Notification due:9/09/2018</li>
      <li>Camera-ready due:30/09/2018</li>
      <li>Conference date:12-14/11/2018</li>                
    </ul>

      <!-- <input type="text" class="form-control" placeholder="Search for...">
      <span class="input-group-btn">
        <button class="btn btn-secondary" type="button">Go!</button>
      </span> -->
    </div>
  </div>
</div>

<!-- Categories Widget -->
<div class="card my-4 border-0">
  <h5 class="card-header"><i class="fas fa-globe"></i> Sponsorship</h5>
  <div class="card-body">
  <div class="row text-center text-lg-left">

    <div class="col-md-6 col-xs-12 mb-4">
      <a href="https://www.nuk.edu.tw/" class="d-block h-100">
        <img class="img-fluid" src="/img/nuk.jpg" alt="sponsor">
      </a>
    </div>
    <div class="col-md-6 col-xs-12 mb-4">
      <a href="https://www.uts.edu.au/" class="d-block h-100">
        <img class="img-fluid" src="/img/uts-logo.jpg" alt="sponsor">
      </a>
    </div>
    <div class="col-md-6 col-xs-12 mb-4">
      <a href="#" class="d-block h-100">
        <img class="img-fluid" src="/img/besc2016.png" alt="sponsor">
      </a>
    </div>
    <div class="col-md-6 col-xs-12 mb-4">
      <a href="http://cis.ieee.org/" class="d-block h-100">
        <img class="img-fluid" src="/img/cis.jpg" alt="sponsor">
      </a>
    </div>
    <div class="col-md-6 col-xs-12 mb-4">
      <a href="http://tasn.org.tw/" class="d-block h-100">
        <img class="img-fluid" src="/img/tasn.jpg" alt="sponsor">
      </a>
    </div>
    <div class="col-md-6 col-xs-12 mb-4">
      <a href="http://snic.nuk.edu.tw/" class="d-block h-100">
        <img class="img-fluid" src="/img/snic.jpg" alt="sponsor">
      </a>
    </div>
    <div class="col-md-6 col-xs-12 mb-4">
      <a href="http://www.nutn.edu.tw/" class="d-block h-100">
        <img class="img-fluid" src="/img/nutn.jpg" alt="sponsor">
      </a>
    </div>
    <div class="col-md-6 col-xs-12 mb-4">
      <a href="https://www.most.gov.tw/" class="d-block h-100">
        <img class="img-fluid" src="/img/most.png" alt="sponsor">
      </a>
    </div>
    <div class="col-md-6 col-xs-12 mb-4">
      <a href="https://www.kcg.gov.tw" class="d-block h-100">
        <img class="img-fluid" src="/img/kao.jpg" alt="sponsor">
      </a>
    </div>
    <div class="col-md-6 col-xs-12 mb-4">
      <a href="https://www.meettaiwan.com/" class="d-block h-100">
        <img class="img-fluid" src="/img/meet.jpg" alt="sponsor">
      </a>
    </div>
    <div class="col-md-6 col-xs-12 mb-4">
      <a href="http://www.ncyu.edu.tw/mis/" class="d-block h-100">
        <img class="img-fluid" src="/img/ncyuim.jpg" alt="sponsor">
      </a>
    </div>
    <div class="col-md-6 col-xs-12 mb-4">
      <a href="https://www.airc.nuk.edu.tw/" class="d-block h-100">
        <img class="img-fluid" src="/img/airc.png" alt="sponsor">
      </a>
    </div>
    <div class="col-md-6 col-xs-12 mb-4">
      <a href="http://biomimedtech.com.tw/intro/super_pages.php?ID=intro1" class="d-block h-100">
        <img class="img-fluid" src="/img/bio.png" alt="sponsor">
      </a>
    </div>
    
    </div>
  </div>
</div>


<!-- Categories Widget -->
<div class="card my-4 border-0">
  <h5 class="card-header"><i class="fas fa-book-open"></i> Publication</h5>
  <div class="card-body">
    <div class="row">
      <img src="img/ieee.png" alt="IEEE">
    </div>
  </div>
</div>

<!-- Side Widget -->
<div class="card my-4 border-0">
  <h5 class="card-header"><i class="fas fa-landmark"></i> Past BESC</h5>
  <div class="card-body">
    <ul>
      <li>
        <a href="http://besc-conf.org/2018/">BESC 2018</a>
      </li>
      <li>
        <a href="http://besc-conf.org/2017/">BESC 2017</a>
      </li>
      <li>
        <a href="http://besc-conf.org/2016/">BESC 2016</a>
      </li>
      <li>
        <a href="http://besc2015.njue.edu.cn/">BESC 2015</a>
      </li>
      <li>
        <a href="http://datamining.it.uts.edu.au/conferences/besc14/">BESC 2014</a>
      </li>
    </ul>

  </div>
</div>

</div>