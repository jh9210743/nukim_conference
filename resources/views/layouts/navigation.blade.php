<!-- Navigation -->

<!-- menu -->
<nav class="cbp-hsmenu-wrapper" id="cbp-hsmenu-wrapper">
      <div class="cbp-hsinner">
        <ul class="cbp-hsmenu">
          <li>
            <a href="/">Home</a>
            <!-- <ul class="cbp-hssubmenu">
              <li><a href="#"><span>Organizing Committee</span></a></li>
              <li><a href="#"><span>Program Committee</span></a></li>
            </ul> -->
          </li>
          <li>
            <a href="#call_for">Call For
              <i class="fas fa-angle-up fa-stack-1x fa-inverse"></i>
              <i class="fas fa-angle-down fa-stack-1x fa-inverse"></i>
            </a>
            <ul class="cbp-hssubmenu cbp-hssub-rows">
              <li><a href="call_for_papers"><span>Call for Papers</span></a></li>
              <li><a href="call_for_industry_track"><span>Call for Industry Track</span></a></li>
              <li><a href="key_dates"><span>Key Dates</span></a></li>
              <li><a href="paper_submission"><span>Paper Submission</span></a></li>
            </ul>
          </li>
          <li>
            <a href="#">Organizers
              <i class="fas fa-angle-up fa-stack-1x fa-inverse"></i>
              <i class="fas fa-angle-down fa-stack-1x fa-inverse"></i>
            </a>
            <ul class="cbp-hssubmenu">
              <li><a href="organizing_committee"><span>Organizing Committee</span></a></li>
              <li><a href="program_committee"><span>Program Committee</span></a></li>
            </ul>
          </li>
          <li>
            <a href="#">Attending
              <i class="fas fa-angle-up fa-stack-1x fa-inverse"></i>
              <i class="fas fa-angle-down fa-stack-1x fa-inverse"></i>
            </a>
            <ul class="cbp-hssubmenu">
              <li><a href="registration"><span>Registration</span></a></li>
              <li><a href="venue"><span>Venue &amp; Accommodation</span></a></li>
              <li><a href="visa"><span>Visa &amp; Travel</span></a></li>
            </ul>
          </li>
          <li>
            <a href="#">Program
              <i class="fas fa-angle-up fa-stack-1x fa-inverse"></i>
              <i class="fas fa-angle-down fa-stack-1x fa-inverse"></i>
            </a>
            <ul class="cbp-hssubmenu">
              <li><a href="program_keynote"><span>Keynotes</span></a></li>
              <li><a href="technical_programme"><span>Technical Programme</span></a></li>
              <li><a href="accepted_papers"><span>Accepted Papers</span></a></li>
            </ul>
          </li>
          <li><a href="contact">Contact</a></li>
        </ul>
      </div>
    </nav>