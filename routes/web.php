<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Home
Route::get('/', function () { return view('index'); });

// Call for
Route::get('/call_for_papers', function () { return view('call_for_papers'); });
Route::get('/call_for_industry_track', function () { return view('call_for_industry_track'); });
Route::get('/key_dates', function () { return view('key_dates'); });
Route::get('/paper_submission', function () { return view('paper_submission'); });

// Oranizers
Route::get('/organizing_committee', function () { return view('organizing_committee'); });
Route::get('/program_committee', function () { return view('program_committee'); });

// Attending
Route::get('/registration', function () { return view('registration'); });
Route::get('/venue', function () { return view('venue'); });
Route::get('/visa', function () { return view('visa'); });

// Program
Route::get('/keynotes', function () { return view('keynotes'); });
Route::get('/technical_programme', function () { return view('technical_programme'); });
Route::get('/accepted_papers', function () { return view('accepted_papers'); });

// Contact
Route::get('/contact', function () { return view('contact'); });
Route::get('/post', function () { return view('post'); });
Route::get('/program_keynote', function () { return view('program_keynote'); });